import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args)
    {
        Persona will = new Persona();
        will.nombre = "William Esteban";
        will.edad = 28;
        will.activo = true;
        will.altura = 1.70;
        will.fecha = new Date();

        System.out.println(will.nombre);
        System.out.println(will.edad);
        System.out.println(will.activo);
        System.out.println(will.altura);
        System.out.println(will.fecha);

        //instanciar la historia clinica

        Historiaclinica historiaClinica = new Historiaclinica();
        historiaClinica.eps = "Sanitas";
        historiaClinica.ips = "San Pedro";
        historiaClinica.persona = will;

        System.out.println(historiaClinica.eps);
        System.out.println(historiaClinica.ips);

        //instaciar visita medica

        Visitamedica visitamedica = new Visitamedica();
        visitamedica.fecha = new Date("2022/01/01");
        visitamedica.diagnostico="hidrocefalia";
        visitamedica.sintomas = "mareo";

        System.out.println(visitamedica.fecha);
        System.out.println(visitamedica.sintomas);
        System.out.println(visitamedica.diagnostico);


        historiaClinica.visitamedicas = new ArrayList<>();
        historiaClinica.visitamedicas.add(visitamedica);

        will.type = "profesor";
        System.out.println(will.trabajar());

        Profesor profesor = new Profesor();
        profesor.trabajar();
        System.out.println(profesor.trabajar());

        Deportista deportista = new Deportista();
        deportista.trabajar();
        System.out.println(deportista.trabajar());

        Bombero bombero = new Bombero();
        bombero.trabajar();
        System.out.println(bombero.trabajar());

        //instanciar 5 personas

        Persona persona1 = new Persona();
        persona1.nombre = "juan";
        persona1.edad = 23;
        persona1.activo = true;
        persona1.altura = 1.72;
        persona1.fecha = new Date();
        Historiaclinica h1 = new Historiaclinica();
        h1.eps = "sanitas";
        h1.ips = "dfsdfsd";


        Persona persona2= new Persona();
        persona2.nombre = "luisa";
        persona2.edad = 18;
        persona2.activo = true;
        persona2.altura = 1.50;
        persona2.fecha = new Date();
        Historiaclinica h2 = new Historiaclinica();
        h2.eps = "sanitas";
        h2.ips = "dfsdfsd";


        Persona persona3 = new Persona();
        persona3.nombre = "Pablo";
        persona3.edad = 45;
        persona3.activo = true;
        persona3.altura = 1.80;
        persona3.fecha = new Date();
        Historiaclinica h3 = new Historiaclinica();
        h3.eps = "sanitas";
        h3.ips = "dfsdfsd";

        Persona persona4 = new Persona();
        persona4.nombre = "Esteban";
        persona4.edad = 45;
        persona4.activo = true;
        persona4.altura = 1.55;
        persona4.fecha = new Date();
        Historiaclinica h4 = new Historiaclinica();
        h4.eps = "sanitas";
        h4.ips = "dfsdfsd";

        Persona persona5 = new Persona();
        persona5.nombre = "Diego";
        persona5.edad = 14;
        persona5.activo = true;
        persona5.altura = 1.75;
        persona5.fecha = new Date();
        Historiaclinica h5 = new Historiaclinica();
        h5.eps = "sanitas";
        h5.ips = "dfsdfsd";


    }
}